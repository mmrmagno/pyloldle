import pygame
import random

pygame.init()

WIDTH, HEIGHT = 1280, 720

YELLOW = (239, 183, 0)
GREEN = (124, 252, 0)
RED = (184, 29, 19)

screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("loldle")
background_image = pygame.image.load("src/bc/summonersrift.jpg")
background_image = pygame.transform.scale(background_image, (WIDTH, HEIGHT))

input_box = pygame.Rect(WIDTH // 2 - 100, HEIGHT // 2 - 260, 140, 30)
color_inactive = pygame.Color('lightskyblue3')
color_active = pygame.Color('dodgerblue2')
color = color_inactive
active = False

#def draw_feedback():
    #x = WIDTH // 5
    #y_offset = HEIGHT // 2 + 50 + scroll_y
    #for guess in previous_guesses:
        #for property, color in guess.items():
            #label_text = pygame.font.Font(None, 20).render(property, True, (0, 0, 0))
            #screen.blit(label_text, (x + 5, y_offset - 20))
            
            #pygame.draw.rect(screen, color, (x, y_offset, 100, 30))
            
            #guessed_property = guessed_champion[property]
            #if type(guessed_property) is list:
                #guessed_property = ", ".join(guessed_property)
            #property_text = pygame.font.Font(None, 20).render(str(guessed_property), True, (0, 0, 0))
            #screen.blit(property_text, (x + 5, y_offset + 5))
            
            #x += 120
        #y_offset += 40
        #x = WIDTH // 5

#def draw_feedback():
    #x = WIDTH // 5
    #y_offset = scroll_y
    #header_drawn = False

    #for guess in previous_guesses:
        #if not header_drawn:
            # Draw the header only once
            #for property in guess:
                #label_text = pygame.font.Font(None, 20).render(property, True, (0, 0, 0))
                #screen.blit(label_text, (x + 5, HEIGHT // 2 + 30 + y_offset))
                #x += 120
            #header_drawn = True
            #y_offset += 40
            #x = WIDTH // 5

        #for property, color in guess.items():
            #pygame.draw.rect(screen, color, (x, HEIGHT // 2 + 50 + y_offset, 100, 30))
            #guessed_property = guessed_champion[property]
            #if type(guessed_property) is list:
                #guessed_property = ", ".join(guessed_property)
            #property_text = pygame.font.Font(None, 20).render(str(guessed_property), True, (0, 0, 0))
            #screen.blit(property_text, (x + 5, HEIGHT // 2 + 55 + y_offset))
            #x += 120
        #y_offset += 40
        #x = WIDTH // 5

def draw_feedback():
    x = WIDTH // 5
    y_offset = HEIGHT // 2 + 50 + scroll_y + input_box.height + 10
    for idx, feedback in enumerate(previous_guesses):

        guessed_image_path = feedback.get("guessed_ico", None)
        if guessed_image_path:
            guessed_image = pygame.image.load(guessed_image_path)
            guessed_image = pygame.transform.scale(guessed_image, (100, 100))
            screen.blit(guessed_image, (x - 110, y_offset))


        for property, color in feedback.items():
            if property == "ico":
                continue
            if not property.startswith("guessed_"):   
                if idx == 0:  
                    label_text = pygame.font.Font(None, 20).render(property, True, (255, 255, 255))
                    screen.blit(label_text, (x + 5, y_offset - 20))
                pygame.draw.rect(screen, color, (x, y_offset, 100, 100))
                
                guessed_property = feedback.get("guessed_" + property, "")
                if type(guessed_property) is list:
                    guessed_property = ", ".join(guessed_property)
                property_text = pygame.font.Font(None, 20).render(str(guessed_property), True, (255, 255, 255))
                screen.blit(property_text, (x + 5, y_offset + 45))
                
                x += 120
        x = WIDTH // 5
        y_offset += 120


champions = {
    "aatrox": {
        "ico": "src/img/aatrox.png",
        "Gender": "Male",
        "Positions": ["Top"],
        "Species": ["Darkin"],
        "Resource": "Manaless",
        "Range type": ["Melee"],
        "Region(s)": ["Shurima", "Runeterra"],
        "Release year": 2013
    },
    "ahri": {
        "ico": "src/img/ahri.png",
        "Gender": "Female",
        "Positions": ["Middle"],
        "Species": ["Vastaya"],
        "Resource": "Mana",
        "Range type": "Ranged",
        "Region(s)": ["Ionia"],
        "Release year": 2011
    },
    "akali": {
        "ico": "src/img/akali.png",
        "Gender": "Female",
        "Positions": ["Middle", "Top"],
        "Species": ["Human"],
        "Resource": "Energy",
        "Range type": ["Melee"],
        "Region(s)": ["Ionia"],
        "Release year": 2010
    },
    "akshan": {
        "ico": "src/img/akshan.png",
        "Gender": "Male",
        "Positions": ["Bottom"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Shurima"],
        "Release year": 2021
    },
    "alistar": {
        "ico": "src/img/alistar.png",
        "Gender": "Male",
        "Positions": ["Support"],
        "Species": ["Minotaur"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Demacia"],
        "Release year": 2009
    },
    "amumu": {
        "ico": "src/img/amumu.png",
        "Gender": "Male",
        "Positions": ["Jungle"],
        "Species": ["Yordle"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Shurima"],
        "Release year": 2009
    },
    "anivia": {
        "ico": "src/img/anivia.png",
        "Gender": "Female",
        "Positions": ["Middle"],
        "Species": ["Cryophoenix"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Freljord"],
        "Release year": 2009
    },
    "annie": {
        "ico": "src/img/annie.png",
        "Gender": "Female",
        "Positions": ["Middle"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Noxus", "Runeterra"],
        "Release year": 2009
    },
    "aphelios": {
        "ico": "src/img/aphelios.png",
        "Gender": "Male",
        "Positions": ["Bottom"],
        "Species": ["Human", "Spiritualist"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Targon"],
        "Release year": 2019
    },
    "ashe": {
        "ico": "src/img/ashe.png",
        "Gender": "Female",
        "Positions": ["Bottom"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Freljord"],
        "Release year": 2009
    },
    "aurelion sol": {
        "ico": "src/img/aurelionsol.png",
        "Gender": "Male",
        "Positions": ["Middle"],
        "Species": ["Celestial", "Dragon"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Runeterra",  "Targon"],
        "Release year": 2016
    },
    "azir": {
        "ico": "src/img/azir.png",
        "Gender": "Male",
        "Positions": ["Middle"],
        "Species": ["God Warrior"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Shurima"],
        "Release year": 2014
    },
    "bard": {
        "ico": "src/img/bard.png",
        "Gender": "Male",
        "Positions": ["Support"],
        "Species": ["Celestial"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Ionia"],
        "Release year": 2015
    },
    "bel veth": {
        "ico": "src/img/belveth.png",
        "Gender": "Female",
        "Positions": ["Jungle"],
        "Species": ["Void Being"],
        "Resource": "Manaless",
        "Range type": ["Melee"],
        "Region(s)": ["The Void"],
        "Release year": 2022
    },
    "blitzcrank": {
        "ico": "src/img/blitzcrank.png",
        "Gender": "N/A",
        "Positions": ["Support"],
        "Species": "Golem",
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Zaun"],
        "Release year": 2009
    },
    "brand": {
        "ico": "src/img/brand.png",
        "Gender": "Male",
        "Positions": ["Mid", "Support"],
        "Species": ["Demon"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["The Abyss"],
        "Release year": 2011
    },
    "braum": {
        "ico": "src/img/braum.png",
        "Gender": "Male",
        "Positions": ["Support"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Freljord"],
        "Release year": 2013
    },
    "camille": {
        "ico": "src/img/camille.png",
        "Gender": "Female",
        "Positions": ["Top"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Piltover"],
        "Release year": 2016
    },
    "cassiopeia": {
        "ico": "src/img/cassiopeia.png",
        "Gender": "Female",
        "Positions": ["Middle"],
        "Species": ["Human", "Half-Serpent"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Noxus"],
        "Release year": 2010
    },
    "cho gath": {
        "ico": "src/img/chogath.png",
        "Gender": "Male",
        "Positions": ["Top"],
        "Species": ["Void Being"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["The Void"],
        "Release year": 2009
    },
    "corki": {
        "ico": "src/img/corki.png",
        "Gender": "Male",
        "Positions": ["Middle"],
        "Species": ["Yordle"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Bandle City"],
        "Release year": 2009
    },
    "darius": {
        "ico": "src/img/darius.png",
        "Gender": "Male",
        "Positions": ["Top"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Noxus"],
        "Release year": 2011
    },
    "diana": {
        "ico": "src/img/diana.png",
        "Gender": "Female",
        "Positions": ["Jungle", "Middle"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Melee"],
        "Region(s)": ["Targon"],
        "Release year": 2012
    },
    "dr.mundo": {
        "ico": "src/img/drmundo.png",
        "Gender": "Male",
        "Positions": ["Top"],
        "Species": ["Mutant"],
        "Resource": "Rage",
        "Range type": ["Melee"],
        "Region(s)": ["Zaun"],
        "Release year": 2009
    },
    "draven": {
        "ico": "src/img/draven.png",
        "Gender": "Male",
        "Positions": ["Bottom"],
        "Species": ["Human"],
        "Resource": "Mana",
        "Range type": ["Ranged"],
        "Region(s)": ["Noxus"],
        "Release year": 2011
    },
    "ekko": {
        "ico": "src/img/ekko.png",
        "Gender": "Male",
        "Positions": ["Middle", "Jungle"],
        "Species": ["Human"],
        "Resource": "Energy",
        "Range type": ["Melee"],
        "Region(s)": ["Zaun", "Piltover"],
        "Release year": 2015
    }

}

values = list(champions.values())
secret_champion = random.choice(values)
print(secret_champion)

current_guess = ""
previous_guesses = []
feedback = {}
scroll_y = 0

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            if input_box.collidepoint(event.pos):
                active = not active

            else: 
                active = False

            color = color_active if active else color_inactive

            if event.button == 4:
                scroll_y += 40
            if event.button == 5:
                scroll_y -= 40

            
            max_scroll = 0
            min_scroll = -(len(previous_guesses) * 120 - HEIGHT + 2 * input_box.height)
            scroll_y = max(min_scroll, min(max_scroll, scroll_y))

        if event.type == pygame.KEYDOWN:
        
            if event.key == pygame.K_BACKSPACE:
                current_guess = current_guess[:-1].lower()

            elif event.key == pygame.K_RETURN:
                if current_guess in champions:
                    guessed_champion = champions[current_guess]
                    #previous_guesses.append((current_guess, feedback))
                    #guessed_image = pygame.image.load(guessed_champion["ico"])  # Load the image
                    #guessed_image = pygame.transform.scale(guessed_image, (100, 100))
                    current_guess = ""
                    feedback = {}     

                    for property, value in guessed_champion.items():
                        feedback["guessed_" + property] = value
                        if type(value) is list:
                            if set(value) & set(secret_champion[property]):
                                feedback[property] = YELLOW if set(value) != set(secret_champion[property]) else GREEN

                            else:
                                feedback[property] = RED

                        else:
                            if value == secret_champion[property]:
                                feedback[property] = GREEN
                        
                            else:
                                feedback[property] = RED
                    previous_guesses.insert(0, feedback)

            else:
                current_guess += event.unicode
    

    screen.fill((255, 255, 255))
    screen.blit(background_image, (0, 0))
    txt_surface = pygame.font.Font(None, 32).render(current_guess, True, (255, 255 ,255))
    width = max(200, txt_surface.get_width()+10)
    input_box.w = width
    screen.blit(txt_surface, (input_box.x+5, input_box.y+5))   
    pygame.draw.rect(screen, color, input_box, 2)
    draw_feedback()
    pygame.display.flip()

pygame.quit()
